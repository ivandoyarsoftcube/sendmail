package main.java.com.devcolibri.model;

public class Product {

    private String date;
    private String title;
    private String listChecks;
    private String flag;

    @Override
    public String toString() {
        return "Statistics{" +
                "date='" + date + '\'' +
                ", title='" + title + '\'' +
                ", listChecks='" + listChecks + '\'' +
                ", flag='" + flag + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product that = (Product) o;

        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (flag != null ? !flag.equals(that.flag) : that.flag != null) return false;
        if (listChecks != null ? !listChecks.equals(that.listChecks) : that.listChecks != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = date != null ? date.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (listChecks != null ? listChecks.hashCode() : 0);
        result = 31 * result + (flag != null ? flag.hashCode() : 0);
        return result;
    }

    public String getDate() {

        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getListChecks() {
        return listChecks;
    }

    public void setListChecks(String listChecks) {
        this.listChecks = listChecks;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
