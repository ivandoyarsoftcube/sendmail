package main.java.com.devcolibri.controller;

import main.java.com.devcolibri.dao.StatisticsDao;
import main.java.com.devcolibri.model.Product;
import java.util.List;

public class StatisticsController {

	private static main.java.com.devcolibri.tls.Sender tlsSender = new main.java.com.devcolibri.tls.Sender("zabbix@softcube.com", "!qA2^BVMTe");

	private StatisticsDao dao;

	public StatisticsController() {
		dao = new StatisticsDao();
	}

	public String getResultHtml(int tenant_id, String date, String listchecks) {

		List<Product> statistices = dao.getListStatistics(tenant_id, date, listchecks);
		StringBuilder stringBuilder = new StringBuilder();

		for (Product statistics : statistices) {

			stringBuilder.append("<td>").append(statistics.getTitle()).append("</td>");
			stringBuilder.append("<td>").append(statistics.getListChecks()).append("</td>");
			if(statistics.getFlag().equals("t"))
				stringBuilder.append("<td bgcolor=\"#ff0000\">").append(statistics.getFlag()).append("</td>");
			else if(statistics.getFlag().equals("f"))
				stringBuilder.append("<td bgcolor=\"#00ff00\">").append(statistics.getFlag()).append("</td>");
			else
				stringBuilder.append("<td>").append(statistics.getFlag()).append("</td>");
		}

		return stringBuilder.toString();
	}

	public String getResultHtmlFlag(int tenant_id, String date, String listchecks) {

		List<Product> statistices = dao.getListStatistics(tenant_id, date, listchecks);
		StringBuilder stringBuilder = new StringBuilder();

		for (Product statistics : statistices) {

			if(statistics.getFlag().equals("t"))
				stringBuilder.append("<td bgcolor=\"#ff0000\">").append(statistics.getFlag()).append("</td>");
			else if(statistics.getFlag().equals("f"))
				stringBuilder.append("<td bgcolor=\"#00ff00\">").append(statistics.getFlag()).append("</td>");
			else
				stringBuilder.append("<td>").append(statistics.getFlag()).append("</td>");
		}

		return stringBuilder.toString();
	}

	public void sendMail() {
		int i;
		long time = System.currentTimeMillis();
		List<Integer> tenants = dao.getListTenants();
		List<String> dates = dao.getListDates();
		StringBuilder result = new StringBuilder();
		result.append("<table border=\"1\"").append("cellspacing=\"0\">").append("<tr>").
		append("<th>tenanttitle</th>").
		append("<th>listchecks</th>");

		for(String date: dates){
			result.append("<th>").append(date).append("</th>");
		}

		result.append("</tr>");
		for(Integer tenant:tenants){
			List<String> listChecks = dao.getTenantListChecks(tenant);
			for(String check:listChecks){
				i=0;
				result.append("<tr>");
				for (String date: dates){
					if(dao.hasCurrentDate(tenant,date) && dao.hasCurrentCheckForCurrentDate(tenant, check, date)){

						if(i > 0){
							result.append(getResultHtmlFlag(tenant, date, check));
						}
						else {
							result.append(getResultHtml(tenant, date, check));
							i++;
						}
					}
					else{
						if(i > 0){
							result.append("<td>").append("").append("</td>");;
	

						}
						else {
							result.append("<td>").append(dao.getTenantTitle(tenant)).append("</td>");
							result.append("<td>").append(check).append("</td>");
							result.append("<td>").append("").append("</td>");
							i++;

						}

					}
				}
				result.append("</tr>");
			}
		}

		result.append("</table>");

		tlsSender.send("Tracking Bugs Statistics",result.toString(),"zabbix@softcube.com", "ivan.doyar@softcube.com");
		tlsSender.send("Tracking Bugs Statistics",result.toString(),"zabbix@softcube.com", "oleg.boroday@softcube.com");
		tlsSender.send("Tracking Bugs Statistics",result.toString(),"zabbix@softcube.com", "irina.schaschenko@softcube.com");
		tlsSender.send("Tracking Bugs Statistics",result.toString(),"zabbix@softcube.com", "oleg.lesov@softcube.com");
		tlsSender.send("Tracking Bugs Statistics",result.toString(),"zabbix@softcube.com", "dima.ivanov@softcube.com");

		dao.closeConnection();
		System.out.print((System.currentTimeMillis()-time)/1000/60);

	}

}
