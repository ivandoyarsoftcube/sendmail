package main.java.com.devcolibri.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbUtil {

    private static Connection connection = null;

    public static Connection getConnection() {

        if (connection != null)
            return connection;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://softcubepostgre02.cloudapp.net:5432/softcube", "admin", "P@$$w0rd88");
            connection.setAutoCommit(false);
            System.out.println("Opened database successfully");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;

    }
}

