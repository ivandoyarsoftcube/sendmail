package main.java.com.devcolibri.dao;

import main.java.com.devcolibri.model.Product;
import main.java.com.devcolibri.util.DbUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StatisticsDao {

    private Connection connection;

    public StatisticsDao() {
        connection = DbUtil.getConnection();
    }
    
    public void closeConnection(){
    	try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    public List<Product> getListStatistics(int tenant_id, String date, String listchecks) {
        List<Product> statisticses = new ArrayList<Product>();

        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("select distinct date, tenanttitle, listchecks, flag from tracking_bugs where id = ? and date = ? and listchecks = ?");
            preparedStatement.setInt(1, tenant_id);
            preparedStatement.setString(2, date);
            preparedStatement.setString(3, listchecks);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                Product statistics = new Product();
                statistics.setDate(rs.getString("date"));
                statistics.setTitle(rs.getString("tenanttitle"));
                statistics.setListChecks(rs.getString("listchecks"));
                statistics.setFlag(rs.getString("flag"));
                statisticses.add(statistics);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return statisticses;
    }

    public List<Integer> getListTenants() {
        List<Integer> tenants = new ArrayList<Integer>();

        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("select distinct tenant_id from sc.product");

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                tenants.add(rs.getInt("tenant_id"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return tenants;
    }

    public List<String> getListDates() {
        List<String> dates = new ArrayList<String>();

        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("select distinct date from tracking_bugs order by date asc");

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                dates.add(rs.getString("date"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return dates;
    }

    public List<String> getTenantListChecks(int tenant) {

        List<String> listChecks = new ArrayList<String>();

        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("select distinct listchecks from tracking_bugs where id = ?");
            preparedStatement.setInt(1, tenant);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                listChecks.add(rs.getString("listchecks"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return listChecks;
    }

	public boolean hasCurrentDate(Integer tenant, String date) {
		
		int i = 0;
		 try {
	            PreparedStatement preparedStatement = connection.
	                    prepareStatement("select date from tracking_bugs where id = ? and date = ?");
	            preparedStatement.setInt(1, tenant);
	            preparedStatement.setString(2, date);
	            ResultSet rs = preparedStatement.executeQuery();

	            while (rs.next()) {
	                i++;
	            }

	        } catch (SQLException e) {
	            e.printStackTrace();
	        }

		 
		 return i!=0;
	}
	
	public boolean hasCurrentCheckForCurrentDate(Integer tenant, String check, String date) {
		
		int i = 0;
		 try {
	            PreparedStatement preparedStatement = connection.
	                    prepareStatement("select date from tracking_bugs where id = ? and date = ? and listchecks = ?");
	            preparedStatement.setInt(1, tenant);
	            preparedStatement.setString(2, date);
	            preparedStatement.setString(3, check);
	            ResultSet rs = preparedStatement.executeQuery();

	            while (rs.next()) {
	                i++;
	            }

	        } catch (SQLException e) {
	            e.printStackTrace();
	        }

		 
		 return i!=0;
	}
	
    public String getTenantTitle(int tenant) {
    	
    	String tenanttitle = null;
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("select distinct tenanttitle from sc.tenant where id = ?");
            preparedStatement.setInt(1, tenant);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                tenanttitle = rs.getString("tenanttitle");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return tenanttitle;
    }

}
